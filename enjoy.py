import argparse
import os
import time
import types

import numpy as np
import torch
from baselines.common.vec_env.dummy_vec_env import DummyVecEnv
from baselines.common.vec_env.vec_normalize import VecNormalize

from common_helpers import make_environment
from envs import WrapPyTorch

parser = argparse.ArgumentParser(description='RL')
parser.add_argument('--seed', type=int, default=1,
                    help='random seed (default: 1)')
parser.add_argument('--num-stack', type=int, default=4,
                    help='number of frames to stack (default: 4)')
parser.add_argument('--log-interval', type=int, default=10,
                    help='log interval, one log per n updates (default: 10)')
parser.add_argument('--env-name', default='PongNoFrameskip-v4',
                    help='environment to train on (default: PongNoFrameskip-v4)')
parser.add_argument('--load-dir', default='./trained_models/',
                    help='directory to save agent logs (default: ./trained_models/)')
parser.add_argument('--add-timestep', action='store_true', default=False,
                    help='add timestep to observations')
parser.add_argument('--total_rewards_output_csv', type=str, default=None)
parser.add_argument('--num-games', type=int, default=None)
parser.add_argument('--visual', dest='visual', action='store_true')
parser.add_argument('--no-visual', dest='visual', action='store_false')
parser.set_defaults(visual=True)
args = parser.parse_args()


# env = make_env(args.env_name, args.seed, 0, None, args.add_timestep)
def make_env():
    env = make_environment(args.env_name, clip_rewards=False, episode_life=False, frame_stack=False)
    env = WrapPyTorch(env)
    return env


env = DummyVecEnv([make_env])

actor_critic, ob_rms = \
    torch.load(os.path.join(args.load_dir, args.env_name + ".pt"))

if len(env.observation_space.shape) == 1:
    env = VecNormalize(env, ret=False)
    env.ob_rms = ob_rms


    # An ugly hack to remove updates
    def _obfilt(self, obs):
        if self.ob_rms:
            obs = np.clip((obs - self.ob_rms.mean) / np.sqrt(self.ob_rms.var + self.epsilon), -self.clipob, self.clipob)
            return obs
        else:
            return obs


    env._obfilt = types.MethodType(_obfilt, env)
    render_func = env.venv.envs[0].render
else:
    render_func = env.envs[0].render

obs_shape = env.observation_space.shape
obs_shape = (obs_shape[0] * args.num_stack, *obs_shape[1:])
current_obs = torch.zeros(1, *obs_shape)
states = torch.zeros(1, actor_critic.state_size)
masks = torch.zeros(1, 1)


def update_current_obs(obs):
    obs = np.array(obs)
    shape_dim0 = env.observation_space.shape[0]
    obs = torch.from_numpy(obs).float()
    if args.num_stack > 1:
        current_obs[:, :-shape_dim0] = current_obs[:, shape_dim0:]
    current_obs[:, -shape_dim0:] = obs


if args.visual:
    render_func('human')
obs = env.reset()
update_current_obs(obs)

if args.env_name.find('Bullet') > -1:
    import pybullet as p

    torsoId = -1
    for i in range(p.getNumBodies()):
        if (p.getBodyInfo(i)[0].decode() == "torso"):
            torsoId = i

total_reward = 0
current_game = 0
if args.total_rewards_output_csv is not None:
    with open(args.total_rewards_output_csv, 'w') as f:
        pass
while True:
    with torch.no_grad():
        value, action, _, states = actor_critic.act(current_obs,
                                                    states,
                                                    masks,
                                                    deterministic=True)
    cpu_actions = action.squeeze(1).cpu().numpy()
    # Obser reward and next obs
    obs, reward, done, _ = env.step(cpu_actions)
    total_reward += reward[0]
    masks.fill_(0.0 if done else 1.0)

    if current_obs.dim() == 4:
        current_obs *= masks.unsqueeze(2).unsqueeze(2)
    else:
        current_obs *= masks
    update_current_obs(obs)

    if args.env_name.find('Bullet') > -1:
        if torsoId > -1:
            distance = 5
            yaw = 0
            humanPos, humanOrn = p.getBasePositionAndOrientation(torsoId)
            p.resetDebugVisualizerCamera(distance, yaw, -20, humanPos)

    if args.visual:
        render_func('human')
        time.sleep(.02)
    if done:
        current_game += 1
        print('total reward of game', current_game, ':', total_reward)
        if args.total_rewards_output_csv is not None:
            with open(args.total_rewards_output_csv, 'a') as f:
                f.write("{}\n".format(total_reward))
        if args.num_games is not None and current_game >= args.num_games:
            print("Done")
            break
        total_reward = 0
        obs = env.reset()
